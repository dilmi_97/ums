const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "test_DB",
});

// !check db connection
db.connect((err) => {
  if (err) {
    throw err;
  } else {
    console.log("success");
  }
});

//****........................................
app.listen(5001, () => {
  console.log("running on port 5001");
});
//****........................................

//?? USER OPERATIONS START....................................

app.get("/getUserData", (req, res) => {
  db.query("SELECT * FROM user", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.get("/getUserData_LastID", (req, res) => {
  db.query(
    "SELECT * FROM user ORDER BY reg_date DESC LIMIT 1",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.post("/getUserData_ID", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const iduser = req.body.idVal;

  db.query(
    "SELECT user.iduser, user.fn,user.ln, user.email,user.password,user.status,channel.channel_name,channel.idchannel FROM user_channel INNER JOIN user ON user.iduser = user_channel.user_id INNER JOIN channel ON user_channel.channel_id = channel.idchannel WHERE user.iduser = ?",
    [iduser],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.post("/updateUser", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const iduser = req.body.idVal;
  const fn = req.body.fn;
  const ln = req.body.ln;
  const status = req.body.status;
  const channel = req.body.channel;
  const email = req.body.email;
  const password = req.body.password;
  const role = req.body.role;
  const dateTime = new Date();

  var sql =
    "UPDATE user SET fn=?,ln=?,password=?,email=?,status=?,reg_date=? WHERE iduser=?";
  var values = [fn, ln, password, email, status, dateTime, iduser];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
    console.log("updated");
    db.query(
      "UPDATE user_channel SET channel_id=?,date=? WHERE user_id =?",
      [channel, "111/11/11", iduser],

      (err, result2) => {
        if (err) {
          console.log(err);
        } else {
          res.send("inserted");
          var sql = "DELETE FROM user_role WHERE user_id = ?";
          var val = [iduser];
          db.query(sql, val, function (err, result) {
            if (err) throw err;
            console.log("Number of records deleted: " + result.affectedRows);

            role.forEach((role) => {
              const sql =
                "INSERT INTO user_role (role_id,user_id,status,date) VALUES (?,?,?,?)";
              db.query(
                sql,
                [role, iduser, status, "111111"],
                (error, result3) => {
                  if (error) {
                    console.log(error.message);
                  } else {
                    console.log(result3);
                  }
                }
              );
            });
          });
        }
      }
    );
  });
});

app.post("/getUserRoleList", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const iduser = req.body.idVal;

  db.query(
    // "SELECT role.role_id, role.role_name FROM user_role INNER JOIN role ON role.role_id = user_role.role_id WHERE user_role.user_id = ?",
    "SELECT role.role_id,role.role_name FROM user_role INNER JOIN role ON role.role_id = user_role.role_id WHERE user_role.user_id = ?",
    [iduser],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.post("/userRemoveMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const dateTime = new Date();

  deleteID.forEach((deleteID) => {
    const sql = "UPDATE user SET status=?, reg_date=? WHERE iduser=?";
    db.query(sql, ["Inactive", dateTime, deleteID], (error, result3) => {
      if (error) {
        console.log(error.message);
      } else {
        // console.log(result3);
        const sql = "UPDATE user_channel SET status=?, date=? WHERE user_id=?";
        db.query(sql, ["Inactive", dateTime, deleteID], (error, result4) => {
          if (error) {
            console.log(error.message);
          } else {
            console.log(result4);
          }
        });
      }
    });
  });
});

app.post("/insertNewUser", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);
  const fn = req.body.fn;
  const ln = req.body.ln;
  const status = req.body.status;
  const channel = req.body.channel;
  const email = req.body.email;
  const password = req.body.password;
  const role = req.body.role;
  const dateTime = new Date();

  var sql = "INSERT INTO user(fn,ln,password,email,status,reg_date) VALUES ?";
  var values = [[fn, ln, password, email, status, dateTime]];
  db.query(sql, [values], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.insertId);
    db.query(
      "INSERT INTO user_channel(channel_id,user_id,status,date) VALUES (?,?,?,?)",
      [channel, result.insertId, status, dateTime],

      (err, result2) => {
        if (err) {
          console.log(err);
        } else {
          res.send("inserted");
          role.forEach((role) => {
            const sql =
              "INSERT INTO user_role (role_id,user_id,status,date) VALUES (?,?,?,?)";
            db.query(
              sql,
              [role, result.insertId, status, dateTime],
              (error, result3) => {
                if (error) {
                  console.log(error.message);
                } else {
                  console.log(result3);
                }
              }
            );
          });
        }
      }
    );
  });
});

//?? USER OPERATIONS END........................................

//?? ROLE OPERATIONS START......................................

app.get("/getRoleList_Modified", (req, res) => {
  db.query(
    "SELECT * FROM role ORDER BY role_date DESC LIMIT 1",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.get("/getRoleList", (req, res) => {
  db.query("SELECT * FROM role", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.post("/roleRemoveMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const dateTime = new Date();

  deleteID.forEach((deleteID) => {
    const sql = "UPDATE role SET role_status=?,role_date=? WHERE role_id=?";
    db.query(sql, ["Inactive", dateTime, deleteID], (error, result3) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log(result3);
      }
    });
  });
});

app.post("/updateRole", (req, res) => {
  // * catch variable values from frontend
  // console.log("role data= " + req.body.permissions);

  const Rid = req.body.Rid;
  const Rname = req.body.Rname;
  const Rdesc = req.body.Rdesc;
  const Rstatus = req.body.status;
  // const permissions = req.body.permissions;
  const dateTime = new Date();

  var sql =
    "UPDATE role SET role_name=?,role_desc=?,role_date=?,role_status=? WHERE role_id=?";
  var values = [Rname, Rdesc, dateTime, Rstatus, Rid];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
  });
});

app.post("/getRoleIdData", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const roleid = req.body.RoleidVal;

  db.query("SELECT * FROM role WHERE role_id = ?", [roleid], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
      console.log(result);
    }
  });
});

app.post("/insertRole", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const roleN = req.body.role;
  const info = req.body.info;
  const status = req.body.status;
  const permissions = req.body.permissions;
  const dateTime = new Date();

  var sql = "SELECT * FROM role WHERE role_name=?";
  var values = [roleN];

  db.query(sql, [values], function (err, row) {
    if (err) {
      console.log(err);
      return;
    } else {
      if (row && row.length) {
        // console.log("Case row was found!");
        // do something with your row variable
        res.send("exists");
      } else {
        var sql =
          "INSERT INTO role(role_name,role_date,role_status,role_desc) VALUES ?";
        var values = [[roleN, dateTime, status, info]];
        db.query(sql, [values], function (err, result) {
          if (err) throw err;
          console.log("Number of records inserted: " + result.insertId);

          permissions.forEach((permissions) => {
            const sql =
              "INSERT INTO role_permission (prole_id,permission_id,pstatus,pdate) VALUES (?,?,?,?)";
            db.query(
              sql,
              [result.insertId, permissions, "Active", dateTime],
              (error, result3) => {
                if (error) {
                  console.log(error.message);
                } else {
                  console.log(result3);
                }
              }
            );
          });
        });
      }
    }
  });
});

//?? ROLE OPERATIONS END.........................................

//?? CHANNEL OPERATIONS START..................................

app.get("/getChannelList_Modified", (req, res) => {
  db.query(
    "SELECT * FROM channel ORDER BY channel_date DESC LIMIT 1",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.get("/getChannelList", (req, res) => {
  db.query("SELECT * FROM channel", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.post("/removeChannelMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const dateTime = new Date();

  deleteID.forEach((deleteID) => {
    const sql =
      "UPDATE channel SET channel_status=?,channel_date=? WHERE idchannel=?";
    db.query(sql, ["Inactive", dateTime, deleteID], (error, result3) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log(result3);
      }
    });
  });
});

app.post("/insertChannel", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const channelN = req.body.channel;
  const info = req.body.info;
  const status = req.body.status;
  const dateTime = new Date();

  var sql = "SELECT * FROM channel WHERE channel_name=?";
  var values = [channelN];

  db.query(sql, [values], function (err, row) {
    if (err) {
      console.log(err);
      return;
    } else {
      if (row && row.length) {
        // console.log("Case row was found!");
        // do something with your row variable
        res.send("exists");
      } else {
        var sql =
          "INSERT INTO channel(channel_name,channel_date,channel_status,channel_desc) VALUES ?";
        var values = [[channelN, dateTime, status, info]];
        db.query(sql, [values], function (err, result) {
          if (err) throw err;
          console.log("Number of records inserted: " + result.insertId);
        });
      }
    }
  });
});

app.post("/getChannelIdData", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const channelid = req.body.ChannelidVal;

  db.query(
    "SELECT * FROM channel WHERE idchannel = ?",
    [channelid],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.post("/updateChannel", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const Cid = req.body.Cid;
  const Cname = req.body.Cname;
  const Cdesc = req.body.Cdesc;
  const Cstatus = req.body.status;
  const dateTime = new Date();

  var sql =
    "UPDATE channel SET channel_name=?,channel_desc=?,channel_date=?,channel_status=? WHERE idchannel=?";
  var values = [Cname, Cdesc, dateTime, Cstatus, Cid];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
    console.log("updated");
  });
});

//?? CHANNEL OPERATIONS END......................................

//?? PERMISSION OPERATIONS START................................

app.get("/getPermission", (req, res) => {
  db.query(
    "SELECT permission_category FROM permission_type GROUP BY permission_category",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

app.post("/getPermission_name", (req, res) => {
  // * catch variable values from the frontend
  console.log(req.body.pCategory);
  const pCategory = req.body.pCategory;

  db.query(
    "SELECT * FROM permission_type WHERE permission_category=?",
    [pCategory],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        // console.log("test" + result);
      }
    }
  );
});

app.post("/getPermission_roleID", (req, res) => {
  // * catch variable values from the frontend
  console.log("test role id " + req.body.RoleID);
  const roleID = req.body.RoleID;

  db.query(
    "SELECT role_permission.pdate,role.role_id,role.role_name,permission_type.permission_type_id,role_permission.pstatus,permission_type.permission_category,permission_type.permission_name FROM role_permission INNER JOIN role ON role.role_id = role_permission.prole_id INNER JOIN permission_type ON role_permission.permission_id = permission_type.permission_type_id WHERE role.role_id = ? ORDER BY pdate DESC",
    [roleID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        // console.log("test" + result);
      }
    }
  );
});

app.post("/assignPermission", (req, res) => {
  // * catch variable values from frontend

  const roleID = req.body.roleID;
  const PermissionID = req.body.PermissionID;
  const dateTime = new Date();

  PermissionID.forEach((PermissionID) => {
    const sql =
      "INSERT INTO role_permission (prole_id,permission_id,pstatus,pdate) SELECT * FROM (SELECT ? AS prole_id, ? AS permission_id, ? AS pstatus, ? AS pdate ) AS tmp WHERE NOT EXISTS ( SELECT * FROM role_permission WHERE prole_id = ? AND permission_id = ?)";

    db.query(
      sql,
      [roleID, PermissionID, "Active", dateTime, roleID, PermissionID],
      (error, result3) => {
        if (error) {
          console.log(error.message);
        } else {
          console.log(result3);
        }
      }
    );
  });
});

app.post("/insertPermission", (req, res) => {
  // * catch variable values from frontend
  const Category = req.body.Category;
  const Permission = req.body.Permission;
  const dateTime = new Date();

  Permission.forEach((Permission) => {
    const sql =
      "INSERT INTO permission_type (permission_category, permission_name,permission_status,permission_date) SELECT * FROM (SELECT ? AS category, ? AS name, ? AS statuss, ? AS date ) AS tmp WHERE NOT EXISTS ( SELECT * FROM permission_type WHERE permission_category = ? AND permission_name = ?)";
    db.query(
      sql,
      [Category, Permission, "Active", dateTime, Category, Permission],
      (error, result) => {
        if (error) {
          console.log(error.message);
        }
      }
    );
  });
  res.send("success");
});

app.post("/assignedPermissionRemove", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const roleID = req.body.roleID;
  const dateTime = new Date();

  const sql =
    "UPDATE role_permission SET pstatus=?,pdate=? WHERE prole_id=? AND permission_id=?";
  db.query(sql, ["Inactive", dateTime, roleID, deleteID], (error, result3) => {
    if (error) {
      console.log(error.message);
    } else {
      console.log(result3);
    }
  });
});

app.post("/removePermission", (req, res) => {
  // * catch variable values from the frontend
  const category = req.body.categoryVal;
  const deletePermissionID = req.body.deletePermissionID;
  //   const category = req.body.categoryVal;
  var array = deletePermissionID.join(",");

  var str = `SELECT permission_type_id FROM permission_type WHERE permission_category = '${category}' AND `;

  var names = array.split(",");
  names.map((o, i) => {
    names[i] = "%" + o + "%";
    str += "permission_name NOT LIKE ? ";
    i == names.length - 1 ? (str += "") : (str += "AND ");
  });

  // console.log(str); //SELECT * FROM table WHERE name LIKE ? OR name LIKE ? OR name LIKE ?
  // console.log(names); //[ '%money%', '%invest%', '%stocks%' ]

  db.query(str, names, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      if (result && result.length) {
        // console.log(result[0].permission_type_id);

        const sql2 = "DELETE FROM permission_type WHERE permission_type_id=?";
        db.query(sql2, [result[0].permission_type_id], (error, result3) => {
          if (error) {
            console.log(error.message);
          } else {
            res.send("deleted");
          }
        });
      }
    }
  });
});

//?? CHANNEL PERMISSIONS END......................................

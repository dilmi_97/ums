import React from "react";
import MainTabPanel from "./components/mainTabPanel/mainTabPanel";

function App() {
  return (
    <React.Fragment>
      <MainTabPanel />
    </React.Fragment>
  );
}

export default App;

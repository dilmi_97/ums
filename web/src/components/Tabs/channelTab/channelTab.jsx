import React, { useState } from "react";
import {
  Grid,
  Row,
  Col,
  Modal,
  ButtonToolbar,
  IconButton,
  Whisper,
  Tooltip,
} from "rsuite";
import CreateBtn from "../../buttons/createBtn";
import { DeleteOutline, Cached, AddModerator } from "@mui/icons-material";
import RemoveBtn from "../../buttons/removeBtn";
import $ from "jquery";
import Swal from "sweetalert2";
import Axios from "axios";
import TableChannel from "../../Tables/tableChannel";
import ModalChannelNew from "../../modal/channel/modalChannelNew";
import ModalChannelUpdate from "../../modal/channel/modalChannelUpdate";

export default function ChannelTab() {
  const [modalChannelNew, setmodalChannelNew] = useState(false);
  const [reloadTable2, setreloadTable2] = useState(0);
  const [channelID, setchannelID] = useState();
  const [modalChannelUpdate, setmodalChannelUpdate] = useState(false);

  //change status of selected channels (deleteBTn) start
  const channelMultiDeleteBtn = () => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    let deleteID = [];
    deleteID = $('input[name="removeChannelId"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();

    if (deleteID.length === 0) {
      let timerInterval;
      Swal.fire({
        title: "Select Record(s)",
        html: "Please! select record(s) to delete.",
        timer: 3000,
        showConfirmButton: false,
        timerProgressBar: true,
        icon: "error",
        willClose: () => {
          clearInterval(timerInterval);
        },
      }).then((result) => {});
    } else {
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#079577",
        cancelButtonColor: "#fd7e14",
        confirmButtonText: "Yes, delete all!",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(
            `${PROTOCOL}://${HOST}:${SERVER_PORT}/removeChannelMulti`,
            {
              deleteID: deleteID,
            }
          ).then((result) => {
            //  setdeleteID(result.data);
          });
          Swal.fire("Deleted!", "Your Record has been deleted.", "success");
          setreloadTable2((key) => key + 1);
        }
      });
    }
  };
  //change status of selected channels (deleteBTn) end

  //open modal for insert new channel start
  const channelNewBtn = () => {
    setmodalChannelNew(true);
  };
  //open modal for insert new channel end

  //close modal for insert new channel start
  const modalChannelNewClose = () => setmodalChannelNew(false);
  //close modal for insert new channel end

  //table refresh Btn start
  const refreshBtn = () => {
    setreloadTable2(0);
  };
  //table refresh Btn end

  //get new function from child component start
  const reloadTable = () => {
    setreloadTable2((key) => key + 1);
  };
  //get new function from child component end

  //show channel edit modal start
  const showUpdateChannelModal = () => {
    $('input[name="Radios"]:checked').each(function () {
      setchannelID(this.value);

      $(".radioImg").prop("checked", false);
      $(this).prop("checked", true);

      setmodalChannelUpdate(true);
    });
  };
  //show channel edit modal end

  //close channel edit modal start
  const modalChannelUpdateClose = () => setmodalChannelUpdate(false);
  //close channel edit modal end

  return (
    <React.Fragment>
      <div className="modal-container">
        <Modal size="sm" open={modalChannelNew} onClose={modalChannelNewClose}>
          <ModalChannelNew reloadTable={reloadTable} />
        </Modal>

        <Modal
          size="sm"
          open={modalChannelUpdate}
          onClose={modalChannelUpdateClose}
        >
          <ModalChannelUpdate channelID={channelID} reloadTable={reloadTable} />
        </Modal>
      </div>

      <Grid fluid className="mb-4">
        <Row className="show-grid">
          <Col xs={6} style={{ display: "inline-flex" }}>
            <CreateBtn
              startIcon={<AddModerator />}
              className="createBtn"
              onClick={channelNewBtn}
            />
            <RemoveBtn
              startIcon={<DeleteOutline />}
              className="removeBtn"
              onClick={channelMultiDeleteBtn}
            />
          </Col>
          <Col xs={6} xsPush={12} style={{ paddingLeft: "160px" }}>
            <Whisper
              placement="top"
              controlId="control-id-hover"
              trigger="hover"
              speaker={<Tooltip>Table Refresh</Tooltip>}
            >
              <ButtonToolbar>
                <IconButton
                  icon={<Cached />}
                  size="xs"
                  appearance="primary"
                  circle
                  className="refresh_icon"
                  onClick={refreshBtn}
                />
              </ButtonToolbar>
            </Whisper>
          </Col>
        </Row>
      </Grid>
      <TableChannel
        removeChannelId="removeChannelId"
        reloadTable2={reloadTable2}
        editClick={showUpdateChannelModal}
        editName="Radios"
        editClassName="radioImg"
      />
    </React.Fragment>
  );
}

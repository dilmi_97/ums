import React, { useState, useEffect } from "react";
import Axios from "axios";
import $ from "jquery";
import Swal from "sweetalert2";
import "../channel/modalChannelUpdate.css";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import StatusPicker from "../../inputFields/statusPicker";
import HiddenInputField from "../../inputFields/hiddenInputField";
import { MessageOutlined,AddModerator } from "@mui/icons-material";
import { Modal, Grid, Whisper, Tooltip, Form, InputGroup, Input } from "rsuite";
 
export default function ModalChannelUpdate({ channelID, reloadTable }) {
  const [status, setStatus] = useState("");
  const [select, setSelect] = useState();
  const [txtarea, settxtarea] = useState();
  const [ChannelData, setChannelData] = useState([]);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  //load Channel Details for Selected channel id start
  useEffect(() => {
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getChannelIdData`, {
      ChannelidVal: channelID,
    }).then((result) => {
      setChannelData(result.data);
      console.log(ChannelData);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  //load Channel Details for Selected channel id end

  //reset btn  start
  const resetBtn = () => {
    setSelect(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };
  //reset btn end

  //channel update btn start
  const channelUpdateBtn = () => {
    if (
      $("#channelName").val() === "" ||
      $("#channelInfo").val() === "" ||
      $("#statusVal").val() === "0"
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/updateChannel`, {
        Cname: $("#channelName").val(),
        Cdesc: $("#channelInfo").val(),
        status: $("#statusVal").val(),
        Cid: channelID,
      }).then(() => {});
      reloadTable();
    }
  };

  //channel update btn end

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <div className="modalHeading">
            <AddModerator /> Channel Info
          </div>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Grid fluid={true} faded="true" className="roleModal">
          {ChannelData.map((val, key) => {
            return (
              <React.Fragment>
                <InputGroup className="InputGroup">
                  <InputGroup.Addon>
                    <MessageOutlined />
                  </InputGroup.Addon>
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      style={{ width: 200 }}
                      placeholder="Channel name"
                      type="text"
                      id="channelName"
                      name="inputFields"
                      defaultValue={val.channel_name}
                    />
                  </Whisper>
                </InputGroup>

                <InputGroup className="InputGroup">
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      as="textarea"
                      name="inputFields"
                      rows={3}
                      defaultValue={val.channel_desc}
                      value={txtarea}
                      id="channelInfo"
                      placeholder="Description"
                      onChange={(event) => {
                        settxtarea(event);
                      }}
                    />
                  </Whisper>
                </InputGroup>

                <Form.Group controlId="radioList">
                  <HiddenInputField
                    name="inputFields"
                    id="statusVal"
                    value={status === "" ? val.channel_status : status}
                  />
                  <StatusPicker
                    value={select}
                    defaultValue={
                      val.channel_status === "Active" ? "Active" : "Inactive"
                    }
                    onChange={(event) => {
                      setStatus(event);
                      setSelect();
                    }}
                    onClean={() => {
                      setStatus("0");
                    }}
                  />
                </Form.Group>
              </React.Fragment>
            );
          })}
        </Grid>
      </Modal.Body>

      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={channelUpdateBtn} value="Update" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}

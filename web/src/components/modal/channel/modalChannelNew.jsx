import React, { useState } from "react";
import Axios from "axios";
import $ from "jquery";
import Swal from "sweetalert2";
import "../channel/modalChannelUpdate.css";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import StatusPicker from "../../inputFields/statusPicker";
import { MessageOutlined, AddModerator } from "@mui/icons-material";
import { Modal, Grid, Whisper, Tooltip, Form, InputGroup, Input } from "rsuite";
 
export default function ModalChannelNew({ reloadTable }) {
  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  const [select, setSelect] = useState();
  const [txtarea, settxtarea] = useState();
  const [status, setStatus] = useState("");

  //resetBTn start
  const resetBtn = () => {
    setSelect(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };
  //resetBTn end

  //channel insert start
  const channelInsert = () => {
    if (
      $("#channelName").val() === "" ||
      $("#channelInfo").val() === "" ||
      status === ""
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/insertChannel`, {
        channel: $("#channelName").val(),
        info: $("#channelInfo").val(),
        status: status,
      }).then((response) => {

        if (response.data==="exists") {
           Swal.fire({
             position: "top-end",
             icon: "error",
             title: "Record Exist!!",
             showConfirmButton: false,
             timer: 1500,
           });
        }

      });
      reloadTable();
    }
  };
  //channel insert end
  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <h4>
            <AddModerator /> New Channel Info
          </h4>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Grid fluid={true} faded="true" className="roleModal">
          <InputGroup className="InputGroup">
            <InputGroup.Addon>
              <MessageOutlined />
            </InputGroup.Addon>
            <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
              <Input
                style={{ width: 200 }}
                placeholder="Channel name"
                type="text"
                id="channelName"
                name="inputFields"
              />
            </Whisper>
          </InputGroup>
          <InputGroup className="InputGroup">
            <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
              <Input
                as="textarea"
                name="inputFields"
                rows={3}
                value={txtarea}
                id="channelInfo"
                placeholder="Description"
                onChange={(event) => {
                  settxtarea(event);
                }}
              />
            </Whisper>
          </InputGroup>
          <Form.Group controlId="radioList">
            <StatusPicker
              value={select}
              id="statusVal"
              onChange={(event) => {
                setStatus(event);
                setSelect();
              }}
            />
          </Form.Group>
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={channelInsert} value="Submit" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}

import React, { useState } from "react";
import {
  Modal,
  Panel,
  Grid,
  Row,
  Tooltip,
  Input,
  Whisper,
  Col,
  InputGroup,
  Form,
} from "rsuite";
import {
  faUser,
  faUserLock,
  faUserPlus,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import { Animated } from "react-animated-css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import $ from "jquery";
import "./modalUserUpdate.css";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import PswField from "../../inputFields/pswField";
import ChannelPicker from "../../inputFields/channelPicker";
import StatusPicker from "../../inputFields/statusPicker";
import RolePicker from "../../inputFields/rolePicker";
import Swal from "sweetalert2";

export default function ModalUserNew({ reloadTable }) {
  const [status, setstatus] = useState(null);
  const [channel, setChannel] = useState(null);
  const [statusVal, setStatusVal] = useState();
  const [channelVal, setChannelVal] = useState();
  const [roleUserValue, setRoleUserValue] = useState([]);

  //resetBtn
  const resetBtn = () => {
    setChannelVal("");
    setStatusVal("");
    setRoleUserValue([]);

    $('input[name="inputFields"]').val("");
  };
  //resetBtn end

  //new user insert Btn start
  const insertUserBtn = () => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    if (
      $("#fnVal").val() === "" ||
      $("#lnVal").val() === "" ||
      status === null ||
      channel === null ||
      $("#emailVal").val() === "" ||
      $("#pswVal").val() === "" ||
      roleUserValue.length === 0
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/insertNewUser`, {
        fn: $("#fnVal").val(),
        ln: $("#lnVal").val(),
        status: status,
        channel: channel,
        email: $("#emailVal").val(),
        password: $("#pswVal").val(),
        role: roleUserValue,
        // idVal: props.data,
      }).then(() => {
        reloadTable();

        setChannelVal("");
        setStatusVal("");
        setRoleUserValue([]);
        $('input[name="inputFields"]').val("");
      });
    }
  };
  //new user insert Btn end

  //updateBtn end
  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <div className="modalHeading">
            <FontAwesomeIcon icon={faUserPlus} /> New User Info
          </div>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* <PanelGroup> */}
        <Panel header="" shaded className="panel1">
          <Animated
            animationIn="zoomInLeft"
            animationOut="fadeOut"
            isVisible={true}
          >
            <Grid fluid={true}>
              <React.Fragment>
                <Row className="show-grid">
                  <Col xs={12} xsPush={12} md={12}>
                    <label>Email:</label>{" "}
                    <InputGroup className="InputGroup">
                      <InputGroup.Addon>
                        <FontAwesomeIcon icon={faEnvelope} />
                      </InputGroup.Addon>

                      <Whisper
                        trigger="hover"
                        speaker={<Tooltip>Required</Tooltip>}
                      >
                        <Input
                          id="emailVal"
                          placeholder="Email@abc.com"
                          type="text"
                          name="inputFields"
                        />
                      </Whisper>
                    </InputGroup>
                    <label>Password:</label>
                    <InputGroup inside className="InputGroup">
                      <PswField />
                    </InputGroup>
                    <label>Status:</label>
                    <Form.Group controlId="radioList">
                      <StatusPicker
                        value={statusVal}
                        id="statusVal"
                        style={{ width: 300 }}
                        onChange={(event) => {
                          setstatus(event);
                          setStatusVal();
                        }}
                      />
                    </Form.Group>
                  </Col>

                  <Col xs={12} xsPull={12}>
                    <label>Name:</label>
                    <InputGroup className="InputGroup">
                      <InputGroup.Addon>
                        <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                      </InputGroup.Addon>
                      <Whisper
                        trigger="hover"
                        speaker={<Tooltip>Required</Tooltip>}
                      >
                        <Input
                          style={{ width: 200 }}
                          placeholder="Name"
                          type="text"
                          id="fnVal"
                          name="inputFields"
                        />
                      </Whisper>
                    </InputGroup>
                    <label>Username:</label>
                    <InputGroup className="InputGroup">
                      <InputGroup.Addon>
                        <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon>
                      </InputGroup.Addon>
                      <Whisper
                        trigger="hover"
                        speaker={<Tooltip>Required</Tooltip>}
                      >
                        <Input
                          style={{ width: 200 }}
                          placeholder="Username"
                          type="text"
                          id="lnVal"
                          name="inputFields"
                        />
                      </Whisper>
                    </InputGroup>
                    <label>Assign Channel:</label>
                    <Form.Group controlId="channel">
                      <ChannelPicker
                        style={{ width: 300 }}
                        value={channelVal}
                        onChange={(event) => {
                          setChannel(event);
                          setChannelVal();
                        }}
                        id="channelVal"
                        menuMaxHeight={210}
                        placement="topStart"
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row className="show-grid mt-2">
                  <label>Assign role(s):</label>
                  <Form.Group controlId="channel">
                    <RolePicker
                      placement="topStart"
                      onChange={setRoleUserValue}
                      menuMaxHeight={250}
                      value={roleUserValue}
                    />
                  </Form.Group>
                </Row>
              </React.Fragment>
            </Grid>
          </Animated>
        </Panel>
        {/*           
        </PanelGroup> */}
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={insertUserBtn} value="Submit" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}

import React from "react";
import { Button, Stack } from "@mui/material";

export default function CreateBtn({ onClick, startIcon, className }) {
  return (
    <React.Fragment>
      <Stack direction="row" spacing={2}>
        <Button
          variant="contained"
          className={className}
          onClick={onClick}
          startIcon={startIcon}
        >
          Create
        </Button>
      </Stack>
    </React.Fragment>
  );
}

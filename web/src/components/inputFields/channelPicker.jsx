import React, { useState, useEffect } from "react";
import Axios from "axios";
import { SelectPicker } from "rsuite";

export default function ChannelPicker({
  style,
  id,
  onClean,
  defaultValue,
  placement,
  onChange,
  menuMaxHeight,
  value,
}) {
  const [channelList, setchannelList] = useState([]);

  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    Axios.get(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getChannelList`).then(
      (response) => {
        setchannelList(response.data);
      }
    );
  }, []);

  return (
    <React.Fragment>
      <SelectPicker
        className="SelectPicker"
        placeholder="Select Channel"
        value={value}
        id={id}
        menuMaxHeight={menuMaxHeight}
        placement={placement}
        style={style}
        onChange={onChange}
        onClean={onClean}
        defaultValue={defaultValue}
        data={channelList.map((x) => {
          return {
            label: x.channel_name,
            value: x.idchannel,
          };
        })}
      />
    </React.Fragment>
  );
}

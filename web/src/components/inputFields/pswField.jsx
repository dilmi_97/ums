import React from "react";
import PswValidation from "../pswValidation/pswValidation";
import { faEye, faEyeSlash, faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Tooltip, Input, Whisper, InputGroup } from "rsuite";

export default function PswField({ defaultValue }) {
  const [pswVisible, setPswVisible] = React.useState(false);

  //PSW Visible
  const PswShowHide = () => {
    setPswVisible(!pswVisible);
  };
  //PSW visible end

  //PSW validation
  // **   if valid do this............................
  const valid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = "1";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "1";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "0";
  };

  // **   if invalid do this............................

  const invalid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = ".5";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "0";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "1";
  };

  const checkValidate = (e) => {
    const password = e;
    if (password.match(/[A-Z]/) != null) {
      valid("capital", "fa_check", "fa_times");
    } else {
      invalid("capital", "fa_check", "fa_times");
    }
    if (password.match(/[a-z]/) != null) {
      valid("lower", "fa_check", "fa_times");
    } else {
      invalid("lower", "fa_check", "fa_times");
    }

    if (password.match(/[0-9]/) != null) {
      valid("numbers", "fa_check", "fa_times");
    } else {
      invalid("numbers", "fa_check", "fa_times");
    }

    if (password.match(/[!@#$%^&*]/) != null) {
      valid("chars", "fa_check", "fa_times");
    } else {
      invalid("chars", "fa_check", "fa_times");
    }

    if (password.length > 7) {
      valid("more8", "fa_check", "fa_times");
    } else {
      invalid("more8", "fa_check", "fa_times");
    }
  };
  //PSW validation end
  return (
    <React.Fragment>
      <InputGroup.Addon>
        <FontAwesomeIcon icon={faLock}></FontAwesomeIcon>
      </InputGroup.Addon>
      <Whisper
        trigger="focus"
        speaker={
          <Tooltip>
            <PswValidation />
          </Tooltip>
        }
      >
        <Input
          type={pswVisible ? "text" : "password"}
          placeholder="Password"
          className="pswField"
          name="inputFields"
          id="pswVal"
          onChange={checkValidate}
          defaultValue={defaultValue}
        />
      </Whisper>
      <InputGroup.Button onClick={PswShowHide}>
        {pswVisible ? (
          <FontAwesomeIcon icon={faEye} />
        ) : (
          <FontAwesomeIcon icon={faEyeSlash} />
        )}
      </InputGroup.Button>
    </React.Fragment>
  );
}
